# DS Security

This is an example recipe which builds on smaller Autologout, CSP and Password Policy recipes.

These smaller recipes in turn configure a single module as a domain of concern.

This recipe is based on the [Sector Security module](https://drupal.org/project/sector_security) [source](https://git.drupalcode.org/project/sector/-/tree/10.0.x/modules/sector_security?ref_type=heads) as a start point, but makes no promises of matching that module's configuration.

- Depends on Drupal modules Autologout, CSP, Password Policy
- Installs modules Autologout, CSP, Password Policy and submodules
- Imports the default configuration of Autologout and CSP ([recipe.yml: config > import > {autologout, csp}](recipe.yml))
- Imports some Password Policy default configuration ([recipe.yml: config > import > password_policy](recipe.yml))
- Provides a Password Policy ([`config/password_policy.password_policy.ds_security.yml`](config/password_policy.password_policy.ds_security.yml))
- Adjusts Autologout to a 1H timeout ([recipe.yml: config > actions > autologout.settings](recipe.yml))
- Adjusts User settings to permit registration by administrator only ([recipe.yml: config > actions > user.settings](recipe.yml))
